package el_por_favor.cheetahchat;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**Class Name: MainMenu
 * Extends: AppCompatActivity
 * Implements: OnClickListener
 * Resume: This is the main menu of our application. Only one button to Chat activity yet
 *
 * @author Jaime Moreno Quintanar (999132)
 * @version 8.11.2016*/
public class MainMenu extends AppCompatActivity implements View.OnClickListener {

    Button buttonToChat;
    Toolbar toolbar;





    //Smart color
    public static boolean client;

    // UI elements
    Button btnClientMode;
    Button btnServerMode;
    TextView txtModeSelection;
    TextView linetext;








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        buttonToChat = (Button) findViewById(R.id.buttonToChat);
        buttonToChat.setOnClickListener(this);

        setupUI();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonToChat:
                Intent intentChat = new Intent(this, Chat.class);
                startActivity(intentChat);
                break;
            case R.id.btn_client_mode:
   /*             Toast.makeText(this, "Client Mode ON", Toast.LENGTH_SHORT).show();
                this.mInClientMode = true;
                hidePrompt("Client Mode");
                setupWifiDirect(false);
                discoverPeers();*/
                client=true;
                break;
            case R.id.btn_server_mode:
              /*  Toast.makeText(this, "Server Mode ON", Toast.LENGTH_SHORT).show();
                this.mInClientMode = false;
                hidePrompt("Server Mode");
                setupServer();*/
                break;
            default:
                break;
        }
    }





    //smart color





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }





/*
    @Override
    protected void onPause() {
        super.onPause();
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
    }*/

    @Override
    protected void onDestroy() {
      //  shutdownCommunication();
        super.onDestroy();
    }


    private void setupUI() {
        btnClientMode = (Button) findViewById(R.id.btn_client_mode);
        btnServerMode = (Button) findViewById(R.id.btn_server_mode);
        btnClientMode.setOnClickListener(this);
        btnServerMode.setOnClickListener(this);
        txtModeSelection = (TextView) findViewById(R.id.prompt);
        linetext = (TextView) findViewById(R.id.line);
    }

    private void hidePrompt(final String mode) {
        btnClientMode.setVisibility(View.GONE);
        btnServerMode.setVisibility(View.GONE);
        txtModeSelection.setText(mode);
    }
}

