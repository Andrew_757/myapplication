package el_por_favor.cheetahchat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import  java.lang.*;

/**Class Name: Chat
 * Extends: AppCompatActivity
 * Implements: OnClickListener
 * Resume: All functionalities related with UI of the chat will be developed on this activity
 * Each time user sends a message, it will be shown on the screen inside a conversation cloud
 *
 * ListView:
 * https://www.youtube.com/watch?v=v7fULw96rC4&t=192s
 *
 * Error: E/SpannableStringBuilder: SPAN_EXCLUSIVE_EXCLUSIVE spans cannot have a zero length
 * If use SwiftKey!! Change to Android keyboard and the error disappears
 * Error2: If the user write more than 11 lines, the sender button disappears from the screen
 * Maybe I can adjust 9.patch bubble chat
 * Error3: E/Surface: getSlotFromBufferLocked: unknown buffer: 0x7f82779220
 * I don't know how to solve it, I think it's a problem only with Marshmallow
 * Any of the errors avoid the app runs properly
 *
 * @author Jaime Moreno Quintanar (999132)
 * @version 21.11.2016*/



public class Chat extends AppCompatActivity implements View.OnClickListener{

    //Elements declaration
    Toolbar toolbar;
    ImageButton sendMessageButton;
    EditText messageToSend;
    TextView sentMessage;
    ImageButton backArrow;
    //ListView elements declaration
    ListView listMessages;
    ArrayAdapter<String> listAdapter;
    ArrayList<String> list;

    //SQLiteDatabase
    DatabaseHelper userMessagesDb;
    ImageButton settingsButton;


    //Smart color
   // public static boolean client;

    public final static int PORT = 9000;

    // What mode are we in?
    boolean mInClientMode;

    // Are we connected?
    boolean mIsConnected = false;


    // WiFi management
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;


    // Socket connectivity
    Socket mSocket;
    OutputStreamWriter mOutput;
    BufferedReader mInput;





    boolean stop=false;
    String line;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        //set up
        //server
        if(!MainMenu.client){
            Toast.makeText(this, "Server Mode ON", Toast.LENGTH_SHORT).show();
            this.mInClientMode = false;
           // hidePrompt("Server Mode");
            setupServer();
        }

        if(MainMenu.client){
            Toast.makeText(this, "Client Mode ON", Toast.LENGTH_SHORT).show();
            this.mInClientMode = true;
            // hidePrompt("Client Mode");
            setupWifiDirect(false);
            discoverPeers();



        }




        //Element initiation
        sendMessageButton = (ImageButton) findViewById(R.id.sendMessageArrow);
        messageToSend = (EditText)findViewById(R.id.writeMessage);
        sentMessage = (TextView)findViewById(R.id.sentMessage);
        backArrow = (ImageButton) findViewById(R.id.backArrow);
        backArrow.setOnClickListener(this);
        listMessages = (ListView) findViewById(R.id.listMessages);
        list = new ArrayList<>();
        listAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.custom, list);
        listMessages.setAdapter(listAdapter);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //SQLite
        userMessagesDb = new DatabaseHelper(this);
        //MAC address
        WifiManager manager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = manager.getConnectionInfo();
        final String userMAC = info.getMacAddress();
        //set up con

        settingsButton = (ImageButton) findViewById(R.id.settingsChat);
        /**settings.setOnClickListener
         * Show the data stored in the database
         *
         * @author Jaime Moreno Quintanar (999132)
         * @version 24.11.2016
         * */
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor res = userMessagesDb.getAllData();
                if(res.getCount() == 0){
                    showMessage("Error", "Nothing found");
                }

                StringBuffer buffer = new StringBuffer();
                while (res.moveToNext()){
                    buffer.append("Id :"+res.getString(0)+"\n");
                    buffer.append("User :"+res.getString(1)+"\n");
                    buffer.append("Message :"+res.getString(2)+"\n\n");
                }
                showMessage("Data", buffer.toString());

            }
        });

        /**sendMessageButton.setOnClickListener
        * With this method, when the user press sendMessageButton (arrow to send messages),
        * the message is include on the ListView inside a yellow bubble and then the EditText
        * is cleaned. In case the user try to send an empty message, an information toast is shown
        * asking for entering a message
        *
        * @author Jaime Moreno Quintanar (999132)
        * @version 21.11.2016
        * */
        sendMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Answer from StackOverFlow: http://stackoverflow.com/questions/4396376/how-to-get-edittext-value-and-store-it-on-textview
                    String contentSentMessage = messageToSend.getText().toString(); //gets the message

                    if (contentSentMessage.isEmpty()) {
                         Toast.makeText(getApplicationContext(), "Please enter a message", Toast.LENGTH_LONG).show();
                    }else {
                        list.add(contentSentMessage);
                        listAdapter.notifyDataSetChanged();
                        //insert message in database
                        userMessagesDb.insertData(userMAC, contentSentMessage);
                        messageToSend.setText(""); //After using it, we clean messageToSend

                        //client
                        if (MainMenu.client){
                            sendClientMessage(contentSentMessage);
                        }
                        //server
                        if (!MainMenu.client){
                            Log.i("server","sending message");
                            sendClientMessage(contentSentMessage);
                        }

                    }

            }
        });


       /* if(!MainMenu.client){
            try {

                 ServerSocket serverSocket = new ServerSocket(PORT);
                Log.i("Smart Color", "Ready to listen for clients");

                 Socket client = serverSocket.accept();
                Log.i("Smart Color", "Client connected");

                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                while(true){
                    line = in.readLine();
                    if(line!=null){
                        list.add(line);
                        listAdapter.notifyDataSetChanged();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }*/
    }

        /**OnClick
         * Back to main menu when user press backArrow
         *
         * Jaime Moreno Quintanar (999132)
         * 22.11.2016
         * */
    @Override
    public void onClick(View view) {
        switch (view.getId()){


            case R.id.backArrow:
                Intent intentMainMenu = new Intent(this, MainMenu.class);
                startActivity(intentMainMenu);
                break;
            default:
                break;
        }
    }

    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }



    /**
     * Called when new P2P devices become available
     * @param devices
     */
    public void onAvailableP2pDevices(final WifiP2pDevice[] devices) {
        Log.i("SmartColor", "Number of peers discovered: " + devices.length);
        if (mIsConnected) {
            // if we are already connected, we do nothing
            return;
        }

        if (devices.length < 1) {
            // sanity check
            return;
        }

        if (mInClientMode) {
            // let us try to connect to the server
            new Thread() {
                @Override
                public void run() {
                    connectToServer(devices[0]);
                }
            }.start();
        }
    }

    private void setupWifiDirect(final boolean isServer) {
        // necessary setup for P2P mode
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        WifiP2pConfig config = new WifiP2pConfig();
        if (isServer) config.groupOwnerIntent = 15;

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);
    }

    private void discoverPeers() {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                Toast.makeText(Chat.this, "Success: FOUND PEERS", Toast.LENGTH_SHORT).show();
                mManager.requestPeers(mChannel, new WifiP2pManager.PeerListListener() {
                    @Override
                    public void onPeersAvailable(WifiP2pDeviceList peers) {
                        Chat.this.onAvailableP2pDevices(
                                peers.getDeviceList().toArray(new WifiP2pDevice[peers.getDeviceList().size()]));
                    }
                });
            }

            @Override
            public void onFailure(int reasonCode) {
                Toast.makeText(Chat.this, "Failure: NO PEERS", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /**
     * Setup server socket to listen for peers.
     */
    private void setupServer() {
        setupWifiDirect(true);
        new Thread() {
            public void run() {
                try {

                    final ServerSocket serverSocket = new ServerSocket(PORT);
                    Log.i("Smart Color", "Ready to listen for clients");

                    final Socket client = serverSocket.accept();
                    Log.i("Smart Color", "Client connected");

                    final OutputStreamWriter out = new OutputStreamWriter(client.getOutputStream());
                    final BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));

                    while(!stop) {
                        Log.i("ANdesr","while looop bitches");
                         line = in.readLine();

                        if(line!=null){

                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(getApplicationContext(), "hola soy el puto amo", Toast.LENGTH_LONG).show();

                                list.add(line);
                                listAdapter.notifyDataSetChanged();
                                //insert message in database
                                userMessagesDb.insertData("mac", line);
                                messageToSend.setText(""); //After using it, we clean messageToSend


                            }

                        });

                        }
                    }


                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }.start();
    }

    /**
     * Connect to the server
     * @param device the server device
     */
    private void connectToServer(WifiP2pDevice device) {
        Log.i("Smart Color", "Connecting to server");
        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.groupOwnerIntent = 0;
        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                mManager.requestConnectionInfo(mChannel, new WifiP2pManager.ConnectionInfoListener() {
                    @Override
                    public void onConnectionInfoAvailable(final WifiP2pInfo wifiP2pInfo) {
                        new Thread() {
                            @Override
                            public void run() {
                                final InetAddress address = wifiP2pInfo.groupOwnerAddress;
                                mSocket = new Socket();
                                Log.i("conection to server" , "successfull");

                                try {
                                    mSocket.connect((new InetSocketAddress(address, PORT)), 2000);



                                    mOutput = new OutputStreamWriter(mSocket.getOutputStream());
                                    mOutput.write("Hello server I am a client 1\n");
                                    mOutput.flush();
                                    mInput = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
                                    //String lastresponse="";
                                    while(!stop){
                                        Log.i("while loop", "client");
                                        try {
                                            sleep(1000);
                                            Log.i("5 sec","try read");
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        } if(mInput.ready()) {
                                            String response = mInput.readLine();
                                            Log.i("readline reads", response);
                                            Log.i("readline", "");

                                                list.add(response);
                                                listAdapter.notifyDataSetChanged();


                                        }


                                    }

                                } catch (FileNotFoundException e) {
                                    e.printStackTrace();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }.start();
                    }
                });
            }

            @Override
            public void onFailure(int reason) {
                //failure logic
            }
        });
    }


    private void shutdownCommunication() {
        try {
            if (mInput != null) mInput.close();
            if (mOutput != null) mOutput.close();
            if (mSocket != null) mSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mIsConnected = false;
    }

/*
    @Override
    protected void onResume() {
        super.onResume();
        if (mReceiver != null) {
            registerReceiver(mReceiver, mIntentFilter);
        }
    }

*/

    private void sendClientMessage(String message){
        try{
            mOutput.write(message+"\n");
            mOutput.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
