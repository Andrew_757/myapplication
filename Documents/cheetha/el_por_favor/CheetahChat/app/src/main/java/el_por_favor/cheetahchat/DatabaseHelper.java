package el_por_favor.cheetahchat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/** DatabaseHelper class
 * Class to manage the database methods. We will use the database to store the messages
 * Code from: https://www.youtube.com/watch?v=cp2rL3sAFmI&list=PLS1QulWo1RIaRdy16cOzBO5Jr6kEagA07
 * @author Jaime Moreno Quintanar
 * @version 23.11.2016
 */

class DatabaseHelper extends SQLiteOpenHelper{

    /*VARIABLES DECLARATION
    * DATABASE_NAME will be the name what our database file will have
    * TABLE_NAME is the name of the table inside our database where we will store the elements
    * Our Table will have 3 fields: ID, name of the sender and the message
    * We are not going to include a COL_1="ID" because in the method onCreate, we will create an
    * autoincrement key that automatically generates the position of each element*/
    private static final String DATABASE_NAME = "Messages.db";
    private static final String TABLE_NAME = "messages_table";
    private static final String COL_1 = "ID";

    private static final String COL_2 = "SENDER";
    private static final String COL_3 = "MESSAGE";

    /*DataBaseHelper constructor*/
    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }
    /*execSQL method execute a single SQL statement that is NOT a SELECT
    or any other SQL statement that returns data. We include the type of each variable. With the
    string ID INTEGER PRIMARY KEY AUTOINCREMENT, our ID will be automatically incremented */
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "SENDER TEXT, MESSAGE TEXT)");
    }
    /*This method drops the table*/
    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
    /*Method to insert the elements in our table. How the Id is automatically incremented,
    * we only have to include the sender and the message
    * With getWritableDatabase() we create and/or open a database that will be used
    * for reading and writing.
    * We create a new ContentValues object where we'll store the values for sender and message
    * for each element in the table. Then we insert contentValues
    * on the table*/
    boolean insertData(String sender, String message){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues =new ContentValues();
        contentValues.put(COL_2, sender);
        contentValues.put(COL_3, message);
        long result = db.insert(TABLE_NAME, null, contentValues);

        return result != -1;
    }
    /*This method shows all the data stored in our table*/
    public Cursor getAllData(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME, null);
        return res;
    }
    public Integer deleteData (String id){
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(TABLE_NAME, "ID = ?", new String[] {id});
    }

}
