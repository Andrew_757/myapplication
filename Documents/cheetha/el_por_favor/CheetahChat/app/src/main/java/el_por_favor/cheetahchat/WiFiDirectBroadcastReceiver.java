package el_por_favor.cheetahchat;

/**
 * Created by escal on 24-Nov-16.
 */

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pManager;
import android.widget.Toast;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.widget.Toast;

/**
 * Responsible for managing WiFi P2P events.
 *
 * This is a skeleton implementation of the P2P subsystem,
 * that reacts to system events around WiFi direct.
 * More information
 * <a href="https://developer.android.com/training/connect-devices-wirelessly/wifi-direct.html">
 *     on Google Developers page</a>.
 *
 * Created by mariusz on 7/07/15.
 */
public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private WifiP2pManager.Channel mChannel;
    private Chat mActivity;

    WifiP2pManager.PeerListListener myPeerListListener;


    /**
     * Broadcast receiver for WiFi P2P notifications.
     *
     * @param manager
     * @param channel
     * @param activity
     */
    public WiFiDirectBroadcastReceiver(final WifiP2pManager manager,
                                       final WifiP2pManager.Channel channel,
                                       final Chat activity) {
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            // Check to see if Wi-Fi is enabled and notify appropriate activity
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);

            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                Toast.makeText(this.mActivity, "WiFi P2P IS enabled.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this.mActivity, "WiFi P2P is NOT enabled.", Toast.LENGTH_SHORT).show();
            }

        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            // Ignore
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            // TODO Respond to new connection or disconnections
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // TODO Respond to this device's wifi state changing
        }
    }

}
