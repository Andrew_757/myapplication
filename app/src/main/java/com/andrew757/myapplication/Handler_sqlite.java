package com.andrew757.myapplication;

/**
 * Created by escal on 16-Sep-16.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;

import  static android.provider.BaseColumns._ID;


public class Handler_sqlite extends SQLiteOpenHelper {

    public Handler_sqlite(Context ctx)
    {
        super(ctx,"location",null,1);
    }

    @Override
    public  void onCreate(SQLiteDatabase db){
        String query = "CREATE TABLE location ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "longitude TEXT, latitude TEXT);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int v_old, int v_new){

        db.execSQL("DROP TABLE IF EXITS location");
        onCreate(db);
    }

    public void insertReg(String lon, String lat){
        ContentValues value =new ContentValues();
        value.put("longitude",lon);
        value.put("latitude",lat);
        this.getWritableDatabase().insert("location",null, value);
    }

    public void openSQL(){
        this.getWritableDatabase();
    }

    public  void closeSQL(){
        this.close();
    }

    public String read(){
        String result="";
        String columns[]={_ID,"longitude","latitude"};
        Cursor c = this.getReadableDatabase().query("location", columns,null,null,null,null,null);

        int id,lon,lat;

       // id=c.getColumnIndex(_ID);
        lon= c.getColumnIndex("longitude");
        lat=c.getColumnIndex("latitude");
        c.moveToLast();
        result =/*c.getString(id)+" "+*/c.getString(lat)+","+c.getString(lon);
        return result;
    }

}
