package com.andrew757.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONException;
import org.json.JSONObject;

public class Activity_2 extends AppCompatActivity {

    private Button b;
    private TextView t;
    private RequestQueue requestQueue;
    private ImageView barney;
    private CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_2);

        b=(Button) findViewById(R.id.button);
        t=(TextView) findViewById(R.id.textView);
        barney=(ImageView) findViewById(R.id.imageView);
        checkBox=(CheckBox) findViewById(R.id.checkBox);

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               if(isChecked==true){
                   barney.setVisibility(View.VISIBLE);
               }
               if(isChecked==false){
                   barney.setVisibility(View.INVISIBLE);
               }

            }
        });


        requestQueue = Volley.newRequestQueue(this);
        b.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Handler_sqlite helper = new Handler_sqlite(Activity_2.this);
                helper.openSQL();

                JsonObjectRequest request= new JsonObjectRequest("https://maps.googleapis.com/maps/api/geocode/json?latlng="+helper.read().toString()+"&key=AIzaSyAPXxLyPTEFI_crhxgTv5kIIA0zrxDj838",
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                   String address = response.getJSONArray("results").getJSONObject(0).getString("formatted_address");

                                    t.setText(address);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },new Response.ErrorListener(){

                        @Override
                    public void onErrorResponse(VolleyError error){
                }
                        });
                        requestQueue.add(request);
                        helper.closeSQL();
            }
        });
    }
}
