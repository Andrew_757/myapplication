package the9thfloor.daletapdale;

import android.content.DialogInterface;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import java.io.File;

/*Class Name: HighScore
* Extends: AppCompatActivity
* Implements: OnClickListener
* Resume: The HighScore activity display the best score you have made in the game and the
* picture you took when you made the high score, it also includes an option to delete the high score
* it uses SQLite and the code is based on this tutorial: https://www.youtube.com/watch?v=AMbxfEdGl8M
* @author Andrés Escalante (998917)
* @version 3.10.2016*/

public class Highscore extends AppCompatActivity {

    /**Elements declaration*/
    public ImageView picture;
    public TextView highscore;
    public Button delete;
    public String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_highscore);

        /**Elements initiation*/
        highscore = (TextView) findViewById(R.id.highscore);
        delete = (Button) findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAlert(getString(R.string.warning_title),getString(R.string.warning_message));
            }
        });
        /*The handler is created to acces the database*/
        HandlerSQLite helper = new HandlerSQLite(Highscore.this);
        helper.openSQL();
        try{
            highscore.setText(helper.read());
        } catch (SQLException|CursorIndexOutOfBoundsException e){

            highscore.setText(R.string.text_high_score_number);
        }
        helper.closeSQL();

        /**Elements initiation*/
        picture = (ImageView) findViewById(R.id.picture);
        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath()+ "/record"+".jpg";
        picture.setImageDrawable(Drawable.createFromPath(path));

    }
    
    /**Method Name: showAlert
     Resume: Given two strings, the method creates a new AlertDialog which confirm that you want
     to delete your high score, if affirmative the function deletes it
     Andrés Escalante Ariza (998917)
     @param  title (String), message (String)
     @see AlertDialog, Builder
     6.10.2016*/
    public void showAlert(String title, String message){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.setPositiveButton(getString(R.string.possitive), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getApplicationContext().deleteDatabase("highscore");
                        File file = new File(path);
                        if (file.delete()) {
                            picture.setImageDrawable(null);
                            highscore.setText(R.string.text_high_score_number);
                        }
                    }
                });

        builder.show();
    }
}

