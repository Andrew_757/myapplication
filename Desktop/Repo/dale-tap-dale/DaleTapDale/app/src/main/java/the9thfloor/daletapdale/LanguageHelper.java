package the9thfloor.daletapdale;

import android.content.res.Configuration;
import android.content.res.Resources;

import java.util.Locale;

/**Class name: LanguageHelper
* Resume: Class in charge of changing the language of our application
*
* Resources: code taken from https://blog.guillaumeagis.eu/change-language/
*
* @author Jaime Moreno Quintanar (999132)
* @see Resources, String, Configuration, Locale
* @version 3.10.2016*/
class LanguageHelper {
    /**Method Name: changeLocale
    Resume: We declare and initiate a new Configuration object with a config given by res. Given
    a new locale, we change the configuration to english("en") or spanish ("es") and then update
    the configuration of res

    Jaime Moreno Quintanar (999132)
    @param  res (Resources) String locale
    @see  Resources, String, Configuration, Locale
    3.10.2016*/
    static void changeLocale(Resources res, String locale){
        Configuration configuration;
        configuration = new Configuration(res.getConfiguration());

        switch (locale){
            case "es":
                configuration.locale = new Locale("es");
                break;
            case "en":
                configuration.locale = new Locale("en");
                break;
            default:
                break;
        }
        res.updateConfiguration(configuration, res.getDisplayMetrics());
    }
}
