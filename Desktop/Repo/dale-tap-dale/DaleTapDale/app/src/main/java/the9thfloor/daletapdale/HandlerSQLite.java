package the9thfloor.daletapdale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


import static android.provider.BaseColumns._ID;
/*Class Name: HandlerSQLite
* Extends: SQLiteOpenHelper
* Resume: Creation of the table and functions to modified it
*
* @author Andrés Escalante (998917)
* @version 3.10.2016*/
class HandlerSQLite extends SQLiteOpenHelper {

        HandlerSQLite(Context ctx) {
            super(ctx,"highscore",null,1);
        }

        @Override
        public  void onCreate(SQLiteDatabase db){
            String query = "CREATE TABLE highscore ("+_ID+" INTEGER PRIMARY KEY AUTOINCREMENT,"+ " bestscore TEXT);";
            db.execSQL(query);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int v_old, int v_new){

            db.execSQL("DROP TABLE IF EXITS highscore");
            onCreate(db);
        }

         /*Method Name: insertReg
         Resume: insert the highscore to the table
         @author Andrés Escalante Ariza (998917)
         @param int score
         @version 3.10.2016*/

        void insertReg(int score){
            ContentValues value =new ContentValues();
            value.put("bestscore",score);
            this.getWritableDatabase().insert("highscore",null, value);
        }

        /*Method Name: openSQL
        Resume: open the SQl database
        @author Andrés Escalante Ariza (998917)
        @version 3.10.2016*/
        void openSQL(){
            this.getWritableDatabase();
        }

        /*Method Name: closeSQL
        Resume: close the SQl database
        @author Andrés Escalante Ariza (998917)
        @version 3.10.2016*/
        void closeSQL(){
            this.close();
        }

        /*Method Name: read
        Resume: read the SQL database
        @author Andrés Escalante Ariza (998917)
        @version 3.10.2016*/
        String read(){
            String columns[]={_ID,"bestscore"};
            Cursor c = this.getReadableDatabase().query("highscore", columns,null,null,null,null,null);
            int best= c.getColumnIndex("bestscore");
            c.moveToLast();
           return c.getString(best);
        }

    }