package the9thfloor.daletapdale;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import java.io.File;


public class Game extends AppCompatActivity {


    public ImageButton imgButtonDale;
    public Button tryAgain;
    public TextView textScore;
    public TextView textClock;
    public TextView textHighScoreNumber;
    int count = 0;
    int score = 0; //este sera el resultado final




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //check for permissions
        if (ActivityCompat.checkSelfPermission(Game.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(Game.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,}
                        , 10);
            }
        }


        setContentView(R.layout.activity_game);
        textClock = (TextView)findViewById(R.id.timer);
        tryAgain = (Button)findViewById(R.id.tryID);
        textHighScoreNumber = (TextView) findViewById(R.id.TextHighScoreNumber);
        HandlerSQLite helper = new HandlerSQLite(Game.this);
        helper.openSQL();
        try{
            textHighScoreNumber.setText(helper.read());
        } catch (CursorIndexOutOfBoundsException e){
            textHighScoreNumber.setText(" 0");
        }
        helper.closeSQL();
        countScore();
        imgButtonDale.setEnabled(false);
        imgButtonDale.setImageResource(R.drawable.dale_gray);
        tryAgain.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                textClock.setVisibility(View.INVISIBLE);
                tryAgain.setEnabled(false);
                tryAgain.setVisibility(View.INVISIBLE);
                tryAgain.setText(getString(R.string.text_try_again));
                count = 0;//reset counter
                timers();
            }
        });


    }

    public void countScore(){
        imgButtonDale = (ImageButton) findViewById(R.id.daleButton);
        textScore = (TextView) findViewById(R.id.score);
        imgButtonDale.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                count++;
            }
        });
        imgButtonDale.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                    imgButtonDale.setImageResource(R.drawable.dale_pressed);
                } else if (motionEvent.getAction() == android.view.MotionEvent.ACTION_UP) {
                    imgButtonDale.setImageResource(R.drawable.dale);
                }
                return false;
            }

        });
    }
    public void timers(){
        //Ready in 3,2,1...
        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                textScore.setText(getString(R.string.ready)+ " " + millisUntilFinished / 1000);
            }

            public void onFinish() {
                textScore.setText(getString(R.string.go));
                textClock.setVisibility(View.VISIBLE);
                imgButtonDale.setEnabled(true);
                imgButtonDale.setImageResource(R.drawable.dale);
            }
        }.start();

        //Time reamining to tap
        new CountDownTimer(9000, 1000) {

            public void onTick(long millisUntilFinished) {
                textClock.setText(getString(R.string.time_remaining)+ " " + millisUntilFinished / 1000);

            }

            public void onFinish() {
                textClock.setText(getString(R.string.game_over));
                score = count;   // si lo pongo aqui en el instante que la cuenta atras pare,
                // TextScore marcara el valor en ese momento de count
                // Score sera util para compararlo con el highscore, solo almacena el valor de count en este instante
                //cuando acabe la cuenta atras de READY? podremos empezar a utilizar el boton
                textScore.setText(getString(R.string.score)+ " " + count);

                if(checkHighScore()){

                    newRecord(getString(R.string.new_high_score),"\n\n"+getString(R.string.save_high_score)+"\n\n\n");


                }
                imgButtonDale.setEnabled(false); //vuelvo a desactivar el boton para que el usuario no se lie
                imgButtonDale.setImageResource(R.drawable.dale_gray);
                tryAgain.setEnabled(true);
                tryAgain.setVisibility(View.VISIBLE);

            }
        }.start();
    }

    /**Method Name: checkHighScore
     Resume: compares the new score to the high score
     Andrés Escalante Ariza (998917)
     @see HandlerSQLite
     5.10.2016*/
    public boolean checkHighScore(){
        int highscore;
        HandlerSQLite helper = new HandlerSQLite(Game.this);
        helper.openSQL();
        try{
            highscore = Integer.parseInt(helper.read());
        } catch (SQLException|CursorIndexOutOfBoundsException e){
            highscore=0;
        }
        helper.closeSQL();

        return score>highscore;

        }

    /**Method Name: newRecord
     Resume: Given two strings, the method creates a new AlertDialog (that is a box in the same
     activity), with Title given by title and a Message given by message that ask if you want
     to save your high score, if the answer is possitive acces the camera to take a picture and
     saves the high score in SQLite
     Andrés Escalante Ariza (998917)
     @param  title (String), message (String)
     @see AlertDialog, Builder, HandlerSQLite
     8.10.2016*/
    public void newRecord(String title, String message){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setPositiveButton(getString(R.string.yes),new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // accessing to the camera
                Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                //save the picture in the sd card, first get the path and them create and Uri
                File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                String pictureName = "record.jpg";
                File imageFile = new File(pictureDirectory, pictureName);
                Uri pictureUri = Uri.fromFile(imageFile);
                camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri);
                startActivityForResult(camera_intent, 1);
                HandlerSQLite helper = new HandlerSQLite(Game.this);
                helper.openSQL();
                try {
                    helper.insertReg(score);
                    textHighScoreNumber.setText(helper.read());
                } catch (SQLException e){
                    textHighScoreNumber.setText("0");
                }
                helper.closeSQL();
            }
        });

        builder.show();
    }

}