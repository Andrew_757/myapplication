package the9thfloor.daletapdale;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;


/**Class Name: MainMenu
* Extends: AppCompatActivity
* Implements: OnClickListener
* Resume: This is the main menu of our application. We have 4 options:
* Play, high scores,languages and help. Play button bring the user to the game activity, high scores
* shows the record of the user, languages show the language selection activity and help show a box
* with the rules of the game.
* Elements: buttonPlay -> Button to the game activity
* buttonHighScores -> Button that shows records
* buttonSettings -> Button to language selection activity
* buttonHelp -> Button that shows a helper box
* logo -> logo of the app (DTD)
*
* @author Jaime Moreno Quintanar (999132)
* @version 3.10.2016*/
public class MainMenu extends AppCompatActivity implements View.OnClickListener {

    /**Elements declaration*/
    Button buttonPlay;
    Button buttonHighScores;
    Button buttonSettings;
    Button buttonHelp;

    //private ImageView dtdLogo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //The following line delete the bar with the name of the app IN THIS ACTIVITY
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main_menu);

        /**Elements initiation*/
        buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonHighScores = (Button) findViewById(R.id.buttonHighScores);
        buttonSettings = (Button) findViewById(R.id.buttonSettings);
        buttonHelp = (Button) findViewById(R.id.buttonHelp);
        //dtdLogo = (ImageView) findViewById(R.id.dtdLogo);

        /**Call to setOnClickListener to make buttons work*/
        buttonHelp.setOnClickListener(this);
        buttonSettings.setOnClickListener(this);
        buttonPlay.setOnClickListener(this);
        buttonHighScores.setOnClickListener(this);

    }

    /**Method Name: showMessage
    Resume: Given two strings, the method creates a new AlertDialog (that is a box in the same
    activity), with Title given by title and a Message given by message

    Jaime Moreno Quintanar (999132)
    @param  title (String), String message
    @see AlertDialog, Builder
    3.10.2016*/
    public void showMessage(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }

    /**Method Name: onClick
    Resume: Implementation of the OnClickListener interface method. We use a switch with 4 cases
    (one for each button we have). When we want to redirection to other activity we need Intent objects

    Jaime Moreno Quintanar (999132)
    @param v (View)
    @see View, showMessage(), Intent, startActivity
    3.10.2016*/
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonHelp:
                showMessage(getString(R.string.help),getString(R.string.instructions));
                break;
            case R.id.buttonSettings:
                Intent intentSettings = new Intent(this, Settings.class);
                startActivity(intentSettings);
                break;
            case R.id.buttonPlay:
                Intent intentPlay = new Intent(this, Game.class);
                startActivity(intentPlay);
                break;
            case R.id.buttonHighScores:
                Intent intentHighScores = new Intent(this, Highscore.class);
                startActivity(intentHighScores);
                break;
            default:
                break;
        }
    }
}
